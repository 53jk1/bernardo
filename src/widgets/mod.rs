pub mod save_file_dialog;
pub mod tree_view;
pub mod button;
pub mod edit_box;
pub mod no_editor;
pub mod with_scroll;
pub mod main_view;
pub mod editor_widget;
pub mod fuzzy_search;
pub mod generic_dialog;
pub mod text_widget;
pub mod file_tree_view;
pub mod open_buffers_list;
pub mod editor_view;
pub mod spath_tree_view_node;
pub mod spath_list_widget_item;
pub mod dir_tree_view;
pub mod action_triggers_fuzzy_provicer;
pub mod list_widget;
pub mod dump_visualizer_widget;
pub mod code_results_view;
pub mod big_list;
pub mod attention_node;

#[cfg(test)]
mod tests;
